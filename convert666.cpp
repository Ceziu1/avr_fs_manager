#include "convert666.h"
#include "ui_convert666.h"

Convert666::Convert666(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Convert666)
{
    ui->setupUi(this);
}

Convert666::~Convert666()
{
    delete ui;
}


void Convert666::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls()) {
        event->acceptProposedAction();
    }
}

void Convert666::dropEvent(QDropEvent *event)
{
    const QUrl url = event->mimeData()->urls().first();
    QString file_name = url.toLocalFile();
    this->pix.load(file_name);

    if (this->pix.isNull()) {
        ui->convert_button->setEnabled(false);
        ui->img->setText("Drop image here");
        QMessageBox mess(QMessageBox::Critical,
                         "",
                         "Selected file is not an image file.");
        mess.exec();
        return;
    }

    ui->convert_button->setEnabled(true);
    ui->cur_size->setText(QString("%1 x %2px")
                          .arg(this->pix.width())
                          .arg(this->pix.height()));

    ui->img->setPixmap(this->pix.scaled(
                           ui->img->width(),
                           ui->img->height(),
                           Qt::KeepAspectRatio));

    QFile file;
    file.setFileName(file_name);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox mess(QMessageBox::Critical,
                         "",
                         "Can not open file.");
        mess.exec();
        return;
    }
}

void Convert666::on_convert_button_released()
{
    this->pix_data.clear();
    quint16 dest_width = this->ui->img_width->value(),
            dest_height = this->ui->img_height->value(),
            _666color;
    QColor color;
    this->pix_data.append(dest_width & 0xFF);
    this->pix_data.append((dest_width >> 8) & 0xFF);
    this->pix_data.append(dest_height & 0xFF);
    this->pix_data.append((dest_height >> 8) & 0xFF);
//    this->pix_data.append(((dest_width >> 4) & 0xF0) | ((dest_height >> 8) & 0x0F));
//    this->pix_data.append(dest_width & 0xFF);
//    this->pix_data.append(dest_height & 0xFF);

    QPixmap p(this->pix.scaled(dest_width, dest_height));
    for (int x = 0; x < dest_width; ++x) {
        for (int y = 0; y < dest_height; ++y) {
            color = p.toImage().pixelColor(x, y);
            _666color = ((color.red() & 0xFC) << 8) |
                    ((color.green() & 0xFC) << 2) |
                    ((color.blue() & 0xFC) >> 2);
            this->pix_data.append(_666color >> 8);
            this->pix_data.append(_666color & 0xFF);
        }
    }
    emit convertionComplete(this->pix_data);
    delete this;
}

void Convert666::on_img_width_valueChanged(int arg1)
{
    QPixmap p;
    if (this->ui->keep_aspect->isChecked())
        p = this->pix.scaledToWidth(arg1);
    else
        p = this->pix.scaled(arg1, this->ui->img_height->value());
    this->ui->img->setPixmap(p);
    this->ui->img_height->setValue(p.height());
}

void Convert666::on_img_height_valueChanged(int arg1)
{
    QPixmap p;
    if (this->ui->keep_aspect->isChecked())
        p = this->pix.scaledToHeight(arg1);
    else
        p = this->pix.scaled(this->ui->img_width->value(), arg1);
    this->ui->img->setPixmap(p);
    this->ui->img_width->setValue(p.width());
}
