#ifndef CONVERT666_H
#define CONVERT666_H

#include <QWidget>
#include <QMimeData>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QDebug>
#include <QMessageBox>
#include <QPixmap>
#include <QDataStream>

namespace Ui {
class Convert666;
}

class Convert666 : public QWidget
{
    Q_OBJECT

public:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    explicit Convert666(QWidget *parent = nullptr);
    ~Convert666();

private slots:
    void on_convert_button_released();

    void on_img_width_valueChanged(int arg1);

    void on_img_height_valueChanged(int arg1);

signals:
    void convertionComplete(QList<quint8> list);

private:
    Ui::Convert666 *ui;
    QPixmap pix;
    QList<quint8> pix_data;
};

#endif // CONVERT666_H
