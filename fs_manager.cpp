#include "fs_manager.h"

bool FS_Manager::_filename_taken(QString filename, QString extension)
{
    for (const QLinkedList<FS_ToC_Entry> &toc : this->toc)
        for (const FS_ToC_Entry &entry : toc)
            if (entry.name == filename && entry.ext == extension)
                return true;
    return false;
}

void FS_Manager::_save_to_data(QList<quint8> list)
{
    /* Posible scenarios
     * 1. Adding to empty data segment
     * 2. Adding between two files (fragmentation)
     * 3. Adding to end of data segment
     */
    FS_Data* last_chunk = nullptr;
    FS_Data chunk;
    chunk.data.fill(0, 506);
    chunk.flags = 0;
    chunk._sector_check = 0xa5;

    while (!list.empty()) {
        for (quint8& d : chunk.data) { // Add each byte from converted image to data segment
            if (!list.empty())
                d = list.takeFirst();
            else
                break;
        }
        if (this->data.empty() || this->header.first_free_sec == this->data.size()) {
            /* data segment is totally empty
             * or first free chunk is in the end
             */
            chunk.next_chunk_addr = 0;
            chunk.flags = 0;
            this->data.append(chunk);
            last_chunk = &this->data.last();
            ++this->header.first_free_sec;
        } else { // fragmentation
            QList<FS_Data>::iterator cur_itm_it = this->data.begin();
            cur_itm_it += this->header.first_free_sec;
            if (!(cur_itm_it->flags & Flags_DS::Free_Sector)) {
                QMessageBox mess(QMessageBox::Critical, "",
                                 "Writing attempt in used block. Aborting.");
                mess.exec();
                // Brutal termination. Change it in future
                throw 5;
            }
            cur_itm_it->data = chunk.data;
            cur_itm_it->flags = 0;
            cur_itm_it->_sector_check = 0xa5;
            last_chunk = &(*cur_itm_it);

            QList<FS_Data>::iterator next_itm_it(cur_itm_it);
            for (;
                 next_itm_it != this->data.end() || !(next_itm_it->flags & Flags_DS::Free_Sector);
                 ++next_itm_it);
            this->header.first_free_sec += (next_itm_it - cur_itm_it);
            cur_itm_it->next_chunk_addr = (next_itm_it - cur_itm_it);
        }

//            current_itm_it += this->header.first_free_sec;
//            QList<FS_Data>::iterator next_itm_it(current_itm_it);
//            QList<FS_Data>::iterator current_itm_it = this->data.begin();
//            FS_Data* current_itm = &(*current_itm_it);
//            qDebug() << "Item place in memory: " << current_itm;

//            for (int i = 0; i < 50; ++i) {
//                chunk.next_chunk_addr = i;
//                this->data << chunk;
//                current_itm_it = this->data.begin();
//                current_itm_it += i;
//                current_itm = &(*current_itm_it);
//                qDebug() << "Item place in memory: " << current_itm;
//                qDebug() << "Pointer next chunk val: " << current_itm->next_chunk_addr;
//                qDebug() << "IT next chunk val: " << current_itm_it->next_chunk_addr;
//            }
//            for (auto qq: this->data) {
//                qDebug() << qq.next_chunk_addr;
//            }
//            return;
//        } else if (next_itm.hasNext()) {
//            while () {

//            }
//        }
//        else if (next == this->data.end()) {

//        }

    }
    if (last_chunk)
        last_chunk->flags |= Flags_DS::Last_Chunk;
}

//FS_Data *FS_Manager::_next_free_chunk()
//{
//    QList<FS_Data>::iterator current_itm_it = this->data.begin();
//    current_itm_it += this->header.first_free_sec;

//}

void FS_Manager::on_666_conversion_complete(QList<quint8> list)
{
    if (this->toc_iterator->size() == this->max_files_per_toc) {
        QMessageBox mess(QMessageBox::Critical,
                         "","Max files count exceeded.");
        mess.exec();
        return;
    }
    bool ok;
    QString text;

    while(true) {
        text = QInputDialog::getText(nullptr, tr("Create image"),
                                                tr("Please name your file"), QLineEdit::Normal,
                                                "", &ok).toUpper();
        if (ok) {
            if (text.length() > this->max_filename_size)
                text.truncate(this->max_filename_size);
            if (this->_filename_taken(text, QString("IMG"))) {
                QMessageBox mess(QMessageBox::Critical,"",
                                 "This filename is already taken.");
                mess.exec();
                continue;
            }
            FS_ToC_Entry entry;
            entry.name = text;
            entry.ext = QString("IMG");
            entry.first_chunk_addr = 0;
            entry.flags = 0;
            entry.size = qCeil(list.size() / 504.0);
            this->toc_iterator->append(entry);
            this->_save_to_data(list);
            emit update_tree_view(&(*this->toc_iterator));
        }
        return;
    }
}

void FS_Manager::on_file_renamed(quint8 position, QString name)
{
    QLinkedList<FS_ToC_Entry>::iterator i = this->toc_iterator->begin();
    quint8 j = 0;
    while(j < position) {++j; ++i;}
    if (name.length() > this->max_filename_size)
        name.truncate(this->max_filename_size);
    if (this->_filename_taken(name.toUpper(), i->ext)) {
        QMessageBox mess(QMessageBox::Critical,"",
                         "This filename is already taken.");
        mess.exec();
        return;
    }
    i->name = name.toUpper();
    emit update_tree_view(&(*this->toc_iterator));
}

FS_Manager::FS_Manager() : fs_name("AVR_FS"), max_files_per_toc(25), max_filename_size(8)
{
}

void FS_Manager::create_new_fs()
{
    this->header.fs_name = QString(this->fs_name);
    this->header.version = 0x01;
    this->header.ToC_count = 0;
    this->header.first_free_sec = 0;
    this->cur_toc = 0;
    this->header._reserved.resize(500);
    this->header._reserved.fill(0xa5);
}

const FS_Header* FS_Manager::get_header()
{
    return &this->header;
}

bool FS_Manager::add_toc()
{
    if (this->header.ToC_count < 0xFF) {
        QLinkedList<FS_ToC_Entry> v;
//        QRandomGenerator rnd(time(NULL));
        this->header.ToC_count++;
//        for (int i = 0; i < this->max_files_per_toc- 1; ++i) {
//            FS_ToC_Entry t1;
//            t1.ext.resize(3);
//            t1.ext.fill(static_cast<quint8>(rnd.bounded(65, 91)));
//            v.append(t1);
//        }
        this->toc.append(v);
        if(this->toc.size() == 1) {
            this->cur_toc++;
            this->toc_iterator = this->toc.begin();
        }
        return true;
    }
    else
        return false;
}

quint8 FS_Manager::open_fs(QString path)
{
    if (this->file.isOpen())
        return 1;
    this->file.setFileName(path);
    if (!this->file.open(QIODevice::ReadWrite)) {
        std::cout<<"Plik spadl z rowerka"<<std::endl;
        return 2;
    }
    QDataStream stream(&(this->file));
    quint8 i = 0;
    for (QChar c : QString(this->fs_name))
        if (c != static_cast<QChar>(this->header.fs_name[i++]))
            return 3;
    return 0;
}

const QLinkedList<FS_ToC_Entry> &FS_Manager::next_toc()
{
    ++this->toc_iterator;
    if(this->toc_iterator != this->toc.end()) {
        emit tocChanged(++this->cur_toc);
        return *this->toc_iterator;
    }
    --this->toc_iterator;
    return *this->toc_iterator;
}

const QLinkedList<FS_ToC_Entry> &FS_Manager::prev_toc()
{
    if(this->toc_iterator != this->toc.begin()) {
        emit tocChanged(--this->cur_toc);
        return *(--this->toc_iterator);
    } else {
        return *this->toc_iterator;
    }
}

QString FS_Manager::convert_toc_entry_flag(const FS_ToC_Entry *entry)
{
    QString tmp;
    ((entry->flags >> 7) & 0x01) ? tmp.append('r') : tmp.append('-');
    ((entry->flags >> 6) & 0x01) ? tmp.append('w') : tmp.append('-');
    ((entry->flags >> 5) & 0x01) ? tmp.append('x') : tmp.append('-');
    ((entry->flags >> 4) & 0x01) ? tmp.append('r') : tmp.append('-');
    ((entry->flags >> 3) & 0x01) ? tmp.append('w') : tmp.append('-');
    ((entry->flags >> 2) & 0x01) ? tmp.append('x') : tmp.append('-');
    ((entry->flags >> 1) & 0x01) ? tmp.append('r') : tmp.append('-');
    (entry->flags & 0x01) ? tmp.append('w') : tmp.append('-');
    return tmp;
}

QDataStream& operator>>(QDataStream &str, FS_Header &header)
{
    quint8 c;
    header.fs_name.resize(6);
    for (quint8 i = 0; i < header.fs_name.size(); ++i) {
        str>>c;
        header.fs_name.replace(i,1,c);
    }
    str>>header.version
        >>header.ToC_count
        >>header.first_free_sec;
    for (quint16 i = 0; i < sizeof(header._reserved); ++i)
        str>>header._reserved[i];
    return str;
}

QDataStream& operator>>(QDataStream &str, FS_ToC_Entry &toc)
{
    quint8 c;
    //REMEMBER to resize new object before loading
    for (quint8 i = 0; i < toc.name.size(); ++i) {
        str>>c;
        toc.name.replace(i,1,c);
    }
    //REMEMBER to resize new object before loading
    for (quint8 i = 0; i < toc.ext.size(); ++i) {
        str>>c;
        toc.ext.replace(i,1,c);
    }
    str>>toc.flags;
    str>>toc.first_chunk_addr;
    str>>toc.size;
    return str;
}

QDataStream& operator>>(QDataStream &str, FS_Data &data)
{
    //REMEMBER to resize new object before loading
    for (quint16 i = 0; i < data.data.size(); ++i)
        str>>data.data[i];
    str>>data.next_chunk_addr;
    str>>data.flags;
    str>>data._sector_check;
    return str;
}

