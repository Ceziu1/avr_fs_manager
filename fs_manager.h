#ifndef FS_MANAGER_H
#define FS_MANAGER_H

#include <QFile>
#include <iostream>
#include <QDataStream>
#include <QMessageBox>
#include <QLinkedList>
#include <QInputDialog>
#include <QDir>
#include <QtMath>
#include <QRandomGenerator>

enum Flags_DS : quint8 {
    Last_Chunk = 0x01,
    Free_Sector = 0x02
};

struct FS_Header
{
    QString fs_name;//[6];
    quint8 version;
    quint8 ToC_count;
    quint32 first_free_sec;
    QVector<quint8> _reserved;//[500];
};

struct FS_ToC_Entry
{
    QString name;//[8];
    QString ext;//[3];
    quint8 flags;
    quint32 first_chunk_addr;
    quint32 size;
};

struct FS_Data
{
    //4 bytes for size
    //502 bytes for data
    QVector<quint8> data;//[506];
    quint32 next_chunk_addr;
    quint8 flags;
    quint8 _sector_check;
};

class FS_Manager : public QObject
{
    Q_OBJECT

private:
    const QString fs_name;
    const quint8 max_files_per_toc;
    const quint8 max_filename_size;

    friend QDataStream& operator>>(QDataStream & str, FS_Header & header);
    friend QDataStream& operator>>(QDataStream & str, FS_ToC_Entry & toc);
    friend QDataStream& operator>>(QDataStream & str, FS_Data & data);

    bool _filename_taken(QString filename, QString extension);
    void _save_to_data(QList<quint8> list);
//    FS_Data *_next_free_chunk();
signals:
    void tocChanged(quint8 cur);
    void update_tree_view(const QLinkedList<FS_ToC_Entry> *toc);

public slots:
    void on_666_conversion_complete(QList<quint8> list);
    void on_file_renamed(quint8 position, QString name);

public:
    FS_Manager();

    void create_new_fs();
    bool add_toc();
    bool add_entry();
    quint8 open_fs(QString path);

    const QLinkedList<FS_ToC_Entry> &next_toc();
    const QLinkedList<FS_ToC_Entry> &prev_toc();
    const FS_Header *get_header();

    static QString convert_toc_entry_flag(const FS_ToC_Entry *entry);

protected:
    QFile file;
    quint8 cur_toc;
    QLinkedList<QLinkedList<FS_ToC_Entry>>::iterator toc_iterator;

    FS_Header header;
    QLinkedList<QLinkedList<FS_ToC_Entry>> toc;
    QList<FS_Data> data;
};

#endif // FS_MANAGER_H
