#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "fs_manager.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->avr_fs_manager = nullptr;
    this->image666_converter = nullptr;
    this->current_toc = nullptr;
    connect(this, &MainWindow::update_tree_view, this->ui->toc_tree, &ToC_tree::update_toc_list);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionImage_666_triggered()
{
    if (!this->avr_fs_manager) {
        QMessageBox mess(QMessageBox::Critical,
                         "", "No active file.");
        mess.exec();
        return;
    }
    this->image666_converter = new Convert666();
    this->image666_converter->setParent(this, Qt::Dialog);
    this->image666_converter->show();
    connect(this->image666_converter, &Convert666::convertionComplete, this->avr_fs_manager, &FS_Manager::on_666_conversion_complete);
}

void MainWindow::on_file_open()
{
    std::cout<< "Otwieramy pliczek"<<std::endl;
}

void MainWindow::on_file_create()
{
    if (this->avr_fs_manager)
        delete this->avr_fs_manager;
    this->avr_fs_manager = new FS_Manager();
    this->avr_fs_manager->create_new_fs();
    this->ui->fs_files_total_lbl->setText("0");
    this->ui->fs_free_sec_lbl->setText(QString::number(this->avr_fs_manager->get_header()->first_free_sec));
    this->ui->fs_name_lbl->setText("PASS");
    this->ui->fs_reserved_lbl->setText("PASS");
    this->ui->fs_toc_count_lbl->setText("0");
    this->ui->fs_version_lbl->setText(QString::number(this->avr_fs_manager->get_header()->version));
    this->ui->toc_add->setEnabled(true);
    connect(this->avr_fs_manager, &FS_Manager::tocChanged, this, &MainWindow::on_toc_change);
    connect(this->avr_fs_manager, &FS_Manager::update_tree_view, this->ui->toc_tree, &ToC_tree::update_toc_list);
    connect(this->ui->toc_tree, &ToC_tree::file_renamed, this->avr_fs_manager, &FS_Manager::on_file_renamed);
}

void MainWindow::on_toc_change(quint8 c)
{
    this->ui->page_number->setText(QString::number(c));
    if (c == 1)
        this->ui->toc_prev->setEnabled(false);
    if (c == this->avr_fs_manager->get_header()->ToC_count)
        this->ui->toc_next->setEnabled(false);
}

void MainWindow::on_toc_add_clicked()
{
    if (!this->avr_fs_manager->add_toc()) {
        QMessageBox mess(QMessageBox::Critical,
                         "", "Max ToC count exceeded.");
        mess.exec();
        return;
    }
    else if(this->avr_fs_manager->get_header()->ToC_count == 1) {
        /* Only one toc
         *
         * 1. If first toc retrieve it
         * 2. Enable button to next toc
         * 3. Update toc count
         */
        this->current_toc = &this->avr_fs_manager->next_toc();
        this->ui->actionImage_666->setEnabled(true);
        emit this->update_tree_view(this->current_toc);
    } else {
        this->ui->toc_next->setEnabled(true);
    }
    this->ui->fs_toc_count_lbl->setText(QString::number(this->avr_fs_manager->get_header()->ToC_count));
}

void MainWindow::on_toc_next_clicked()
{
    this->current_toc = &this->avr_fs_manager->next_toc();
    this->ui->toc_prev->setEnabled(true);
    emit this->update_tree_view(this->current_toc);
}

void MainWindow::on_toc_prev_clicked()
{
    this->current_toc = &this->avr_fs_manager->prev_toc();
    this->ui->toc_next->setEnabled(true);
    emit this->update_tree_view(this->current_toc);
}
