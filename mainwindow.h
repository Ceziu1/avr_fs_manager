#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLinkedList>
#include <QDebug>

#include "convert666.h"
#include "fs_manager.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionImage_666_triggered();
//    void on_666_conversion_complete(QLinkedList<quint16> list);
    void on_file_open();
    void on_file_create();
    void on_toc_change(quint8 c);

    void on_toc_add_clicked();
    void on_toc_next_clicked();
    void on_toc_prev_clicked();

signals:
    void update_tree_view(const QLinkedList<FS_ToC_Entry> *toc);

private:
    Ui::MainWindow *ui;
    Convert666 *image666_converter;
    FS_Manager *avr_fs_manager;
    const QLinkedList<FS_ToC_Entry> *current_toc;
};
#endif // MAINWINDOW_H
