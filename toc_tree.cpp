#include "toc_tree.h"
#include "ui_toc_tree.h"

ToC_tree::ToC_tree(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ToC_tree)
{
    ui->setupUi(this);
//    connect(this, &ToC_tree::customContextMenuRequested, this, &ToC_tree::on_treeWidget_customContextMenuRequested);
}

ToC_tree::~ToC_tree()
{
    delete ui;
}

void ToC_tree::update_toc_list(const QLinkedList<FS_ToC_Entry> *toc)
{
    this->ui->treeWidget->clear();
    QList<QTreeWidgetItem *> items;
    for (FS_ToC_Entry const &e : *toc) {
        QStringList tmp = {
            QString("%1.%2").arg(e.name).arg(e.ext),
            QString("no data"),
            QString(QString::number(e.size)),
            FS_Manager::convert_toc_entry_flag(&e)
        };
        items.append(new QTreeWidgetItem(static_cast<QTreeWidget *>(nullptr), tmp));
    }
    this->ui->treeWidget->insertTopLevelItems(0, items);
}

void ToC_tree::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    std::cout<< this->ui->treeWidget->indexOfTopLevelItem(item)<<std::endl;
    if (column == 0) {
        bool ok;
        QString text = QInputDialog::getText(nullptr, tr("Rename %1").arg(item->text(0)),
                                                tr("Please add new name"), QLineEdit::Normal,
                                                item->text(0).split('.')[0], &ok);
        if (ok)
            emit file_renamed(this->ui->treeWidget->indexOfTopLevelItem(item), text);
    }
}

void ToC_tree::on_treeWidget_customContextMenuRequested(const QPoint &pos)
{
    QTreeWidgetItem *item = this->ui->treeWidget->itemAt(pos);
    if (!item)
        return;
    QMenu contextMenu(tr("Context menu"), this);

    QAction action1("Remove Data Point", this);
    connect(&action1, SIGNAL(triggered()), this, SLOT(removeDataPoint()));
    contextMenu.addAction(&action1);

    contextMenu.exec(mapToGlobal(pos));
}
