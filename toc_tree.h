#ifndef TOC_TREE_H
#define TOC_TREE_H

#include <QWidget>
#include <QLinkedList>
#include <QTreeWidgetItem>
#include <QDebug>
#include <QMenu>

#include <fs_manager.h>

namespace Ui {
class ToC_tree;
}

class ToC_tree : public QWidget
{
    Q_OBJECT

public:
    explicit ToC_tree(QWidget *parent = nullptr);
    ~ToC_tree();

public slots:
    void update_toc_list(const QLinkedList<FS_ToC_Entry> *toc);

private slots:
    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);
    void on_treeWidget_customContextMenuRequested(const QPoint &pos);

signals:
    void file_renamed(quint8 position, QString name);

private:
    Ui::ToC_tree *ui;
    QMenu ctx;
};

#endif // TOC_TREE_H
